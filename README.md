In this project, there are several scripts in different branches:

-  master branch : compute_txt_len.py
	=> Script modified to respect the python nomenclature (according to pylint)

- Cours branch: select_fasta.py
	=> Script realized in class, allows to search for an identifier in a fasta file and to retrieve its sequence in an output file.
	3 parameters :
		- inputfile : indicate the path of the file containing the fasta sequences.
		- identifiers: indicate the identifiers you are looking for
		- outputfile: indicate the output path for the created file

- Home branch: select_fasta_home.py
	=> Script that does the same thing as the "select_fasta.py" script, in the Courses branch, but done in a different way.

