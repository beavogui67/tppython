#!/usr/bin/env python3
"""
Verification of the script with pylint
"""
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import re

transcript_start = dict()
transcript_end = dict()

def get_tx_genomic_length(input_file=None):
    """
    The function reads a file entered by the user, and displays the identifiers
    of the different transcripts with the longest length.
    None means that it can have nothing in input.
    """

    file_handler = open(input_file)
    for line in file_handler:
        token = line.split("\t")
        start = int(token[3])  # the beginning of the current element
        end = int(token[4])  # At the end of the current element
        # The identifier of the transcript

        tx_id = re.search('transcript_id "([^"]+)"', token[8]).group(1)

        if tx_id not in transcript_start:

            transcript_start[tx_id] = start
            transcript_end[tx_id] = end

        else:

            if start < transcript_start[tx_id]:
                transcript_start[tx_id] = start

            if end > transcript_end[tx_id]:
                transcript_end[tx_id] = end

    for tx_id in transcript_start:
        print(tx_id + "\t" + str(transcript_end[tx_id] - transcript_start[tx_id] + 1))


if __name__ == '__main__':
    get_tx_genomic_length(input_file='../pymetacline/data/gtf/simple.gtf')
